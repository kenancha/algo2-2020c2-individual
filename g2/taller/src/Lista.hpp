#include "Lista.h"

Lista::Lista(): _largo(0),_primElem(NULL),_ultElem(NULL) {
    // Completar

}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    while (_largo>0)
        eliminar(0);
    delete _primElem;
    delete _ultElem;
    //delete largo;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    while (_largo>0)
        eliminar(0);

    Nodo* nuevo = aCopiar._primElem;
    int i =aCopiar._largo;

    while (i>0) {
        agregarAtras(nuevo->val);
        nuevo=nuevo->sig;
        i--;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    Nodo* nodo = new Nodo ;
    nodo->ant = NULL;
    nodo->sig = _primElem;
    nodo->val = elem;
    if (_largo == 0)
        _ultElem = nodo;
    else
        _primElem->ant = nodo;

    _primElem = nodo;

    _largo ++;

}

void Lista::agregarAtras(const int& elem) {
    // Completar
    Nodo* nodo = new Nodo;
    nodo->sig = NULL;
    nodo->val = elem;
    nodo->ant = _ultElem;
    if(_largo == 0)
        _primElem = nodo;
    else
        _ultElem->sig = nodo;
    _ultElem = nodo;
    _largo ++;
    return;
}

void Lista::eliminar(Nat i) {
    // Completar
    if (_largo-1 < i)
    {
        return;
    }
    Nodo* nodo;
    Nodo* anterior;
    Nodo* siguiente;
    nodo= _primElem;

    if (i == 0 ) {
        _primElem = nodo->sig;
        delete nodo;
        if (_largo == 1) {
            _ultElem = NULL;
        } else {
            nodo = _primElem;
            nodo->ant = NULL;
        }
        _largo --;
        return;
    }

    if (i == _largo-1)
    {
        nodo= _ultElem;
        _ultElem = nodo->ant;
        delete nodo;
        nodo = _ultElem;
        nodo->sig = NULL;
        _largo --;
        return;
    }

    while (i>0)// lo tengo que poner en =0?
    {
        nodo=nodo->sig;
        i--;
    }
    anterior = nodo->ant;
    siguiente = nodo->sig;
    anterior->sig = siguiente;
    siguiente->ant = anterior;
    delete nodo;
    _largo --;
}

int Lista::longitud() const {
    return _largo;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
   // assert(false);
    Nodo* nodo = _primElem;
    while (i>0)
    {
        nodo = nodo->sig;
        i--;
    }
    int& res = nodo->val;
    return res;
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
 //   assert(false);
    Nodo* nodo = _primElem;
    while (i>0)
    {
        nodo = nodo->sig;
        i--;
    }
    int& res = nodo->val;
    return res;
}

void Lista::mostrar(ostream& o) {
    // Completar
    o<< "Lista: [";
    for(int i=0; i<_largo;i++){
        if(i+1!=_largo)
            o<<iesimo(i)<<", ";
        o<<iesimo(i);
    }

    o<<" ]";
}
