
#include "Conjunto.h"

template<class T>
Conjunto<T>::Conjunto():_raiz(NULL), _elem(0) {
    // Completar
}

template<class T>
Conjunto<T>::~Conjunto() {
    //
    //Rehacer iterativo/recursivo
    if (_elem > 0) {
        if (_raiz->izq != NULL)
            chauArbol( _raiz->izq);
        if (_raiz->der != NULL)
            chauArbol(_raiz->der);
        delete _raiz;
        _elem--;
    }
}

template<class T>
void Conjunto<T>::chauArbol(Nodo* &nodo) {
    if (nodo->izq != NULL)
        chauArbol(nodo->izq);
    if (nodo->der != NULL)
        chauArbol(nodo->der);
  delete nodo;
  _elem--;
}

template<class T>
bool Conjunto<T>::pertenece(const T &clave) const {
     bool res= false;

    if(_elem == 0)
        return false;
     if (_raiz->valor==clave) {
        return true;
    } else if (clave < _raiz->valor && _raiz->izq != NULL){
        res = pizq(_raiz->izq, clave);
    } else if(clave > _raiz->valor && _raiz->der != NULL) {
        res = pizq(_raiz->der, clave );
    }

        return res;
}


template<class T>
bool Conjunto<T>::pizq(Nodo* &n, const T &clave) const{
    bool res = false;
    if (n->valor==clave) {
        return true;
    } else if(clave < n->valor && n->izq != NULL) {
        res = pizq(n->izq,clave);
    }else if(clave > n->valor && n->der != NULL){
        res = pizq(n->der,clave);
    }
        return res;
}




template<class T>
void Conjunto<T>::insertar(const T &clave) {

    if (_elem == 0) {
        _raiz = new Nodo(clave);
    } else {

        if (pertenece(clave))
            return;
        else if (clave < _raiz->valor)
            izq(_raiz, clave);
        else if (clave > _raiz->valor)
            der(_raiz, clave);
    }
    while(_raiz->padre != NULL)
        _raiz=_raiz->padre;
    _elem ++;
    return;
}

template<class T>
void Conjunto<T>::der(Nodo* &n, const T &clave) {

    if (n->der == NULL) {
        n->der = new Nodo(clave);
        n->der->padre = n;
        return;
    }
        if (clave < n->der->valor){
        izq(n->der, clave);
    } else {
        der(n->der, clave);
    }
}

template<class T>
void Conjunto<T>::izq(Nodo* &n, const T &clave) {
    if (n->izq == NULL) {
        n->izq = new Nodo(clave);
        n->izq->padre = n;
        return;
    } else if (clave < n->izq->valor){
        izq(n->izq, clave);
    } else {
        der(n->izq, clave);
    }
}



template<class T>
void Conjunto<T>::remover(const T &clave) {
    if(!pertenece(clave))
        return;
    if(_raiz->valor==clave) //raiz con menos de 2 hijos
    {
        if (_raiz->der ==NULL && _raiz->izq!=NULL){
            _raiz = _raiz->izq;
            delete _raiz->padre;
            _raiz->padre = NULL;
            _elem--;
            return ;}
        if (_raiz->izq ==NULL && _raiz->der!=NULL)
        {   _raiz = _raiz->der;
            delete _raiz->padre;
            _raiz->padre = NULL;
            _elem--;
            return ;}
        if (_raiz->izq == NULL && _raiz->der == NULL)
        {
            _elem--;
            delete _raiz;
            _raiz=NULL;
            return ;}
    }
    Nodo* raiz;
    raiz= _raiz;
    ubicar(raiz ,clave);
    _elem--;
    //delete &raiz;
    return ;
}

template <class T>
void Conjunto<T>::ubicar(Nodo* &sacar,const T &clave) {

    while (sacar->valor != clave){
    if (sacar->valor > clave)
  sacar=sacar->izq;
    else
    sacar = sacar->der;

    }
    Nodo* cambio = sacar->der;
    if(cambio != NULL) {
    while (cambio->izq != NULL)
        cambio = cambio->izq;
    }
    reemplazo(sacar,cambio);
   //delete &cambio;
}

template<class T>
void Conjunto<T>::reemplazo(Nodo* &sacar, Nodo* &cambio) {

    if (sacar->izq == NULL && sacar->der == NULL) { //hoja, no raiz
        if(sacar->padre->der != NULL){
            if (sacar->padre->der->valor == sacar->valor) {
                sacar->padre->der = NULL;
                delete sacar;
                return;
            } else {
                sacar->padre->izq = NULL;
                delete sacar;
                return;
            }
        }else
            sacar->padre->izq = NULL;
        delete sacar;
        return;
    }


    if(sacar->der == NULL) { //hoja izquierda
        //padre por derecha
        if (sacar->padre->der != NULL) {
            if (sacar->padre->der->valor == sacar->valor) {
                sacar->padre->der = sacar->izq;
                sacar->izq->padre = sacar->padre;
                delete sacar;
                return;
            } else {
                sacar->padre->izq = sacar->izq;
                sacar->izq->padre = sacar->padre;
                delete sacar;
                return;
            }
        } else { //padre por izquierda
            sacar->padre->izq = sacar->izq;
            sacar->izq->padre = sacar->padre;
            delete sacar;
            return;
        }

    } else {

     Nodo* temp = new Nodo(sacar->valor);

     sacar->valor = cambio->valor;
     cambio->valor=temp->valor;
    ubicar(cambio, cambio->valor);
    delete temp;

    return;

    }

}

template<class T>
const T &Conjunto<T>::siguiente(const T &clave) {
   // if (clave> maximo())
     //   return NULL;
    return masProximo(_raiz, clave);

}


template <class T>
const T &Conjunto<T>::masProximo(Nodo* &n, const T &clave) {
    if (n->valor <= clave && n->der != NULL)
        return masProximo(n->der, clave);
    else if (n->valor > clave && n->izq != NULL) {
        if (n->izq->valor == clave && n->izq->der == NULL)
            return n->valor;
        return masProximo(n->izq, clave);
    }else
        return n->valor;

}


template<class T>
const T &Conjunto<T>::minimo() const {
    Nodo* n = _raiz;
    if (_elem> 0)
    {

        while (n->izq != NULL)
        {
            n=n->izq;
        }

    }
    return n->valor;
}

template<class T>
const T &Conjunto<T>::maximo() const {
    Nodo* n = _raiz;
    if (_elem> 0)
    {

        while (n->der != NULL)
        {
            n=n->der;
        }

    }
    return n->valor;
}

template<class T>
unsigned int Conjunto<T>::cardinal() const {
    return _elem;
}

template<class T>
void Conjunto<T>::mostrar(std::ostream &) const {
    assert(false);
}


