// Nombre: Kenaan
// Apellido: Chab
// LU: 458/19
#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

private:
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia): mes_(mes), dia_(dia){}
int Fecha::mes(){
    return mes_;
}
int Fecha::dia(){
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() <<'/'<<f.mes();
    return os;
}






#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();

    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia(){
    if(dias_en_mes(mes_)== dia_ ){
        dia_=1;
        if(mes_==12) {mes_=1;} else {mes_++;}
    } else{
        dia_++;
    }

}


// Ejercicio 11, 12

// Clase Horario
class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario f);
    bool operator<(Horario f);

private:
    uint hora_;
    uint min_;



};
Horario::Horario(uint hora, uint min): hora_(hora), min_(min){}
uint Horario::hora(){
    return hora_;
}
uint Horario::min(){
    return min_;
}

bool Horario:: operator==(Horario f){
    bool min = this->min_ ==f.min_;
    bool hora = this->hora_ ==f.hora_;

    return min && hora;
}

ostream& operator<<(ostream& os, Horario f){
    os<< f.hora() << ":"<< f.min();
    return os;
}


bool Horario::operator<(Horario f){
    bool menor= false;
    if (this->hora_<f.hora_){
        menor =true;
    }else if (this->hora_>f.hora_){
        menor= false;
    } else if (this->min_<f.min_){
        menor=true;
    } else { menor=false;}
    return menor;
}


// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
public:
    Recordatorio(Fecha fecha, Horario horario, string mensaje);
    string mensaje();
    Fecha fecha();
    Horario horario();
private:
    string mensaje_;
    Fecha fec_;
    Horario hor_;
};
string Recordatorio::mensaje(){
    return mensaje_;
}
Fecha Recordatorio::fecha(){
    return fec_;
}
Horario Recordatorio::horario(){
    return hor_;
}
Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje): fec_(fecha), hor_(horario), mensaje_(mensaje) {}

ostream& operator<<(ostream& os, Recordatorio r){
    os<< r.mensaje() << " @ "<< r.fecha() << " " << r.horario();
    return os;
}

//----------------------------------------Me tira error el test-------------------------------------------------------

// Ejercicio 14

// Clase Agenda
class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    list<Recordatorio> citas_;
};

Agenda::Agenda(Fecha fecha_inicial):hoy_(fecha_inicial){}
void Agenda::agregar_recordatorio(Recordatorio rec){

    list<Recordatorio>:: iterator i = citas_.begin();
    for (Recordatorio r: citas_)
    {
        if (rec.horario() < r.horario()) {
            citas_.insert(i, rec);
            return;
        }
        i++;
    }
        citas_.push_back(rec);

}
void Agenda::incrementar_dia(){
    hoy_.incrementar_dia();
}
list<Recordatorio> Agenda::recordatorios_de_hoy(){
    list<Recordatorio> res;

    for (Recordatorio i: citas_)
    {
        if (i.fecha()== hoy_)
        {res.push_back(i);}
    }
    return res;
}
Fecha Agenda::hoy(){
    return hoy_;
}

ostream& operator<<(ostream& os, Agenda a){
    os<< a.hoy() << endl << "====="<< endl;
    for(Recordatorio r: a.recordatorios_de_hoy())
    {
        os<< r << endl;
    }
    return os;
}