template <typename T>
string_map<T>::string_map():_size(0),t(){
    raiz = new Nodo();
}

template <typename T>
string_map<T>::string_map(const string_map<T>&aCopiar) : string_map() { *this = aCopiar; }
// Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>&d) {
    chauTrie(raiz);
    raiz = new Nodo();
    Nodo* n = d.raiz;
    holaTrie(raiz, n);
    /*int i = 255;
    while(i>=0){
        if (d.raiz->siguientes[i] != nullptr) {
            raiz->siguientes[i] = new Nodo();
            holaTrie(raiz->siguientes[i], d.raiz->siguientes[i]);
        }
         i--;
    }*/
    return *this;
}

template <typename T>
void string_map<T>::holaTrie(Nodo* &destino, Nodo* &fuente) {
    if(fuente->definicion != nullptr) {
        destino->definicion = new T(*fuente->definicion);
    }
    int i = 255;
    while(i>=0){
        if (fuente->siguientes[i] != nullptr) {
            destino->siguientes[i] = new Nodo();
            holaTrie(destino->siguientes[i], fuente->siguientes[i]);
        }
        i--;
    }
}



template <typename T>
string_map<T>::~string_map(){
    // COMPLETAR
    //llamar recursivamente a recorrer vectores, eliminar hojas
    chauTrie(raiz);
    /*for(int i = 0; i < 256; i++){
        if(raiz->siguientes[i] != nullptr){
            chauTrie(raiz->siguientes[i]);
        }
    }*/
}
template <typename T>
void string_map<T>::chauTrie(Nodo* tracker){
    for(int i = 0; i < 256; i++){
        if(tracker->siguientes[i] != NULL){
            chauTrie(tracker->siguientes[i]);
        }
    }
    delete tracker->definicion;
    delete tracker;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    string claveIt=clave;
    Nodo* tracker=raiz;
    while (claveIt.length() > 0){
        if(tracker->siguientes[int(claveIt.front())] == nullptr)////wtf
            return 0;
        tracker = tracker->siguientes[int(claveIt.front())];
        claveIt.erase(0,1);
    }
    if(tracker->definicion != NULL)
        return 1;
    return 0; // 1 o 0 (pertenece)
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    string claveIt=clave;
    Nodo* tracker=raiz;
    while (claveIt.length() > 0){
        tracker = tracker->siguientes[int(claveIt.front())];
        claveIt.erase(0,1);
    }

    return  *tracker->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
   string claveIt=clave;
   Nodo* tracker=raiz;
    while (claveIt.length() > 0){
        tracker = tracker->siguientes[int(claveIt.front())];
        claveIt.erase(0,1);
    }
 //   return  *tracker->definicion;
    t = *tracker->definicion;
    return t;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    // COMPLETAR
    string claveIt = clave, ult=clave;
    Nodo* tracker = raiz;
    Nodo* ultConHijos = raiz;
    while (claveIt.length() > 0){
        if(tracker->hijos > 1){
            ultConHijos = tracker; //este borrado corta la cadena y desciende, se puede copiar la clave y hacer el borrado ascendente
            ult=claveIt;

        }
        tracker = tracker->siguientes[int(claveIt.front())];
        claveIt.erase(0,1);
    }
    delete tracker->definicion;
    tracker->definicion = NULL;
    if (tracker->hijos==0)
    {
        tracker = ultConHijos->siguientes[int(ult.front())];
        ultConHijos->hijos--;
        ultConHijos->siguientes[int(ult.front())] = NULL;
        borrarRama(ult, tracker);
    }
    _size--;
}


template <typename T>
void string_map<T>::borrarRama(string &ult, Nodo* tracker){
    ult.erase(0,1);
    while(ult.length() > 0)
    {
        Nodo* ultConHijos = tracker->siguientes[int(ult.front())];
        borrarRama(ult, ultConHijos);

    }
    delete tracker->definicion;
    delete tracker;
}




template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return  _size == 0;
}

template <typename T>
void string_map<T>::insert(const pair<string, T> &p){
    string s = p.first;

    if (raiz->siguientes[int(s.front())] == NULL){
        raiz->siguientes[int(s.front())] = new Nodo();
        raiz->hijos++;
    }
    Nodo* tracker = raiz->siguientes[int(s.front())];
    s.erase(0,1);

    while (s.length() > 0){
        if (tracker->siguientes[int(s.front())] == NULL){
            tracker->siguientes[int(s.front())] = new Nodo();
            tracker->hijos++;
        }
        tracker = tracker->siguientes[int(s.front())];
        s.erase(0,1);
    }
    if (tracker->definicion == NULL){
        tracker->definicion = new T(p.second);
    }else{
        delete tracker->definicion;
        tracker->definicion = new T(p.second);
    }

    _size++;
}



